// подключение пакетов
var gulp = require('gulp'),
    pug  = require('gulp-pug'),
    scss = require('gulp-sass'),
    browserSync = require('browser-sync'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    minifyhtml = require('gulp-minify-html'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');
    
gulp.task('pug', function(){
        return gulp.src('app/pug/index.pug')
        .pipe(pug())
        .pipe(gulp.dest('app'))
});

gulp.task('scss', function(){
        return gulp.src('app/scss/style.scss')
        .pipe(scss())
        .pipe(gulp.dest('app/css'))
});

gulp.task('cssnano', function(){
    return gulp.src('app/css/style.css')
    .pipe(cssnano())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist/css'))
});

gulp.task('minify-html', function(){
    return gulp.src('app/index.html')
    .pipe(minifyhtml())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist'))
});

gulp.task('imagemin', function(){
    return gulp.src('app/img/**/*')
    .pipe(imagemin({
        interlaced: true,
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
    }))
    .pipe(gulp.dest('dist/img'))
});

gulp.task('build', function(){
    return bildFonts = gulp.src('app/fonts/**/*')
       .pipe(gulp.dest('dist/fonts'));
});

gulp.task('browser-sync', function(){
    browserSync({
     server:{
        baseDir: 'app'
     },
        notify: false // отключение уведомлений
    });
});

gulp.task('watch',['browser-sync','scss'], function(){
    gulp.watch('app/scss/style.scss',['scss']);
    gulp.watch('app/index.html', browserSync.reload);
    gulp.watch('app/css/style.css', browserSync.reload);
});